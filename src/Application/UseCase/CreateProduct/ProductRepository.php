<?php

namespace App\Application\UseCase\CreateProduct;

use App\Domain\Model\Product;
use App\Infrastructure\Entity\ProductPersistenceObject;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Репозиторий для доменной модели продукта
 */
class ProductRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function findById(string $id): Product
    {
        /** @var ProductPersistenceObject $object */
        $object = $this->entityManager->find(ProductPersistenceObject::class, $id);

        return new Product(
            $object->getId(),
            $object->getTitle(),
            $object->getCode()
        );
    }

    public function save(Product $product): void
    {
        $persistenceObject = new ProductPersistenceObject(
            $product->getId(),
            $product->getTitle(),
            $product->getCode()
        );

        $this->entityManager->persist($persistenceObject);
        $this->entityManager->flush();
    }
}
