FROM php:7.4-alpine

COPY . .

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN composer global require hirak/prestissimo && composer install
